import { Component, OnInit, ViewChild  } from '@angular/core';
import { ReservationsService} from '../reservations.service';
import { Treatment, Product, Reservations } from '../data-model';
import { CalendarComponent } from 'ng-fullcalendar';
import { Options } from 'fullcalendar/dist/fullcalendar';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss']
})
export class AgendaComponent implements OnInit {
  public reservations;
  public Ereservations;

  public reservations_error:Boolean = false;


 /* calendarOptions:Object = {
    height: 'parent',

    fixedWeekCount : false,
    defaultDate: '2017-10-12',
    editable: true,
    eventLimit: true, // allow "more" link when too many events
    events: []

  };*/


 /* onCalendarInit(initialized: boolean) {

    console.log('Calendar initialized');
  }*/
    calendarOptions: any;
    displayEvent: any;
    logger: any[] = [];
    @ViewChild(CalendarComponent) ucCalendar: CalendarComponent;

  constructor(private _reservationsService: ReservationsService,
              private route: ActivatedRoute,
              private router: Router) { }

  getReservationsInternet() {


  }
  /*calendarOptions(){

    let calendarOptions:Object = {
      height: 'auto',
      defaultView: 'agendaDay',
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay, prevYear,nextYear'
      },
      editable: true,
      eventLimit: true,
      events: this.reservations,
      slotDuration: '00:15:00',
      minTime: '09:00:00',
      themeSystem: 'bootstrap3',
      dow: [ 1, 2, 3, 4, 5 ], // Monday - Thursday

      start: '9:00', // a start time (10am in this example)
      end: '22:30', // an end time (6pm in this example)


     /!* eventClick: function(event, jsEvent, view) {
        alert('Event: ' + event.title + '  ' + 'Start Date: ' + event.start);
      },
      dayClick: function(date, jsEvent, view) {
        alert('Clicked on: ' + date.format());
      }*!/
    };
    return(calendarOptions);
  }*/
  ngOnInit() {
      this._reservationsService.getReservations().subscribe(
          data => {
              this.calendarOptions = {
                  defaultView: 'agendaDay',
                  businessHours: {
                      dow: [ 1, 2, 3, 4, 5 ],
                      start: '10:00',
                      end: '22:00',
                  },
                  eventLimit: false,
                  header: {
                      left: 'prev,next today',
                      center: 'title',
                      right: 'month,agendaWeek,agendaDay,listDay, listMonth'
                  },
                  nowIndicator: true,
                  firstDay: 1,
                  scrollTime: this.getCurrentTime(),
                  minTime: '9:00:00',
                  slotDuration: '00:05:00',
                  events: data,
                  timeFormat: 'H(:mm)',
                  slotLabelFormat: 'H(:mm)',
                  themeSystem: 'bootstrap3',
                  hiddenDays: [ 0, 2 ],
                  timezone: 'Europe/Amsterdam'


              };
              console.log("Reservations :  %o", data);
          },
          err => {
              this.reservations_error = true;

              console.log('Something went wrong getting reservations!', err);
          },
          () => console.log('done loading reservations %o', this.calendarOptions)
      );

      /*this.ucCalendar.fullCalendar('rerenderEvents');*/
  }
  getCurrentTime() {
      var d = new Date();
      return d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

  }
    clickButton(model: any) {
        this.displayEvent = model;
    }
    eventClick(model: any) {
        model = {
            event: {
                id: model.event.id,
                start: model.event.start,
                end: model.event.end,
                title: model.event.title,
                allDay: model.event.allDay
                // other params
            },
            duration: model.event.Duration,
            data: {
                employeenamme: model.event.Employee,
                employeeid: model.event.EmployeeID,
                reservationdetailid: model.event.ReservationDetailID,
            }
        }
        this.displayEvent = model;
        console.log("Display even %o :", this.displayEvent);

    }
    gotoReservationProductDetails() {
        this.router.navigate(['/reservationproductdetails:id']);
    }
    updateEvent(model: any) {
        model = {
            event: {
                id: model.event.id,
                start: model.event.start,
                end: model.event.end,
                title: model.event.title
                // other params
            },
            duration: {
                _data: model.duration._data
            },
            data: {
                employeenamme: model.event.Employee,
                employeeid: model.event.EmployeeID,
                reservationdetailid: model.event.ReservationDetailID,
            }
        }
        this.displayEvent = model;
    }
    windowResize(model: any) {
        console.log('The calendar has adjusted to a window resize');
    }
    viewRender(model: any) {
        console.log('viewRender');
    }
    eventRender(model: any) {
        this.logger.push(model);
    }
    initialized() {
        console.log('Initialized complete');}

}
