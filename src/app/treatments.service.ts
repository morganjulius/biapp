import { Injectable } from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs/observable/of';
import { MessageserviceService } from './messageservice.service';
import { catchError, map, tap } from 'rxjs/operators';
import { Freespace} from './classes/freespace';
import { Treatment } from './data-model';
import { Product } from './data-model';



@Injectable()
export class TreatmentsService {
    public freespace;
    private reservationsUrl = 'https://dev.vectrus.nl/hl/api/v1/reservations/';
    private freespaceUrl = 'https://dev.vectrus.nl/hl/api/v1/freespace/';
    public loading = false;

    private log(message: string) {
        this.messageService.add('Treatment core service: ' + message);
    }

    constructor(
        private http:HttpClient,

        private messageService: MessageserviceService
    ) { }


  getTreatments() {
    return this.http.get('https://dev.vectrus.nl/hl/api/v1/treatments')
        .map((res:Response) => res);



  } // stub
  getTreatment(TreatmentID) {
    return this.http.get('https://dev.vectrus.nl/hl/api/v1/treatment/'+TreatmentID)
        .map((res:Response) => res);




  } // stub

  getFreeSpaces(TreatmentID) {
    /*return this.http.get('https://dev.vectrus.nl/hl/api/v1/freespace/'+TreatmentID)
        .map((res:Response) => res);*/
      this.loading = true;

      const url = `${this.freespaceUrl}${TreatmentID}`;
      return this.http.get<Freespace>(url).pipe(
          tap(_ => this.log(`fetched freespace timeslots for id=${TreatmentID}`)),
          tap(_ => this.loading = false),

          catchError(this.handleError<Freespace>(`Failed fetching Freespace timeslots for treatment id=${TreatmentID}`),

              )
      );


  }
  getAllProducts() {


      return this.http.get<Product>('https://dev.vectrus.nl/hl/api/v1/products').pipe(
          tap(_ => this.log(`fetched products`)),

          catchError(this.handleError<Freespace>(`Failed fetching products`),

          )
      );


  }
  getAllProductsFromTreatment(TreatmentID) {

      return this.http.get<Treatment>('https://dev.vectrus.nl/hl/api/v1/product/'+ TreatmentID).pipe(
          tap(_ => this.log(`fetched treatment for id=${TreatmentID}`)),

          catchError(this.handleError<Freespace>(`Failed fetching treatment id=${TreatmentID}`),

          )
      );





  }
    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

}
