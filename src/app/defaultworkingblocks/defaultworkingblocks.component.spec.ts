import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultworkingblocksComponent } from './defaultworkingblocks.component';

describe('DefaultworkingblocksComponent', () => {
  let component: DefaultworkingblocksComponent;
  let fixture: ComponentFixture<DefaultworkingblocksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultworkingblocksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultworkingblocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
