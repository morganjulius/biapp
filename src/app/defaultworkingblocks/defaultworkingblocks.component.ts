import {Component, OnInit, Input, ViewChild, OnChanges} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {WorkingblocksService} from '../services/workingblocks.service';
import {Workingblock} from '../classes/workingblock';
import {WorkingblocksComponent} from "../workingblocks/workingblocks.component";
import {EmployeesService} from '../services/employees.service';
import {Employee} from '../classes/employee';
import {EmployeesComponent} from "../employees/employees.component";
import {DatePipe} from '@angular/common';
import {CalendarComponent} from 'ng-fullcalendar';
import { Options } from 'fullcalendar';


interface IDay {
    name: string;
    value: string;
    starttime: string;
    endtime: string;
    daynr: string;
}


@Component({
    selector: 'app-defaultworkingblocks',
    templateUrl: './defaultworkingblocks.component.html',
    styleUrls: ['./defaultworkingblocks.component.scss']
})
export class DefaultworkingblocksComponent implements OnInit, OnChanges {
    calendarOptions: Options;

    @Input() workingblock: WorkingblocksComponent;
    @Input() employee: EmployeesComponent;

    public employees: any;

    selectedEmployee: any = Employee;

    public detailsForm: FormGroup;
    events = [];
    displayEvent: any;

    public workingblocks: any;

    selectedWorkingblock: any = Workingblock;

    public workingblocks_error: Boolean = false;

    defaultWorkingblockForm = new FormGroup({
        starttime: new FormControl(),
        endtime: new FormControl()
    });



    logger: any[] = [];
    public reservations_error: Boolean = false;

    @ViewChild(CalendarComponent) ucCalendar: CalendarComponent;

    constructor(private _workingblockService: WorkingblocksService,
                private route: ActivatedRoute) {

        this.route.params.subscribe(params => console.log("Params", params));

    }




    getWorkingblock(workingblockid) {
        console.log('Getting products for this treatment!');
        this._workingblockService.getWorkingblock(workingblockid).subscribe(
            data => {
                this.selectedWorkingblock = data;
                console.log(" Workingblocks details", this.selectedWorkingblock);


            },
            err => {
                this.workingblocks_error = true;
                console.log('Something went wrong!');
            }
        );


    }

    getWorkingblocks(EmployeeID) {
        this._workingblockService.getWorkingblocks(EmployeeID).subscribe(
            data => {
                this.workingblocks = data;
                console.log("workingblocks :  : ", this.workingblocks);


            },
            err => {
                this.workingblocks_error = true;
                console.log('Something went wrong!');
            }
        );

    }

    loadCalendar(EmployeeID) {
        this._workingblockService.getWorkingblocks(EmployeeID).subscribe(
            data => {
                this.calendarOptions = {
                    defaultView: 'agendaWeek',
                    businessHours: {
                        dow: [1, 2, 3, 4, 5],
                        start: '10:00',
                        end: '22:00',
                    },
                    eventLimit: false,
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay,listDay, listMonth'
                    },
                    nowIndicator: true,
                    firstDay: 1,
                    minTime: '9:00:00',
                    editable: true,
                    selectable: true,
                    events: data,
                    timeFormat: 'H:mm',
                    slotLabelFormat: 'H:mm',
                    themeSystem: 'bootstrap3',
                    hiddenDays: [0, 2],
                    timezone: 'Europe/Amsterdam'


                };
                console.log("Workingblocks for employee :  %o", data);

            },
            err => {
                this.reservations_error = true;

                console.log('Something went wrong getting reservations!', err);
            },
            () => console.log('done loading reservations %o', this.calendarOptions)
        );
    }
    ngOnChanges() {
        console.log('Change detected thingy now the id =....%o ', this.employee, ' This %o: ', this.calendarOptions);
        //this.ucCalendar.fullCalendar('removeEvents');
        //this.loadCalendar(this.employee.EmployeeID);
        this.calendarOptions = {
            events: ''
        };
        let employeeid = this.employee.EmployeeID;
        this.loadCalendar(employeeid);
        this.ucCalendar.fullCalendar('renderEvents', this.events);
        //this.ucCalendar.fullCalendar('refetchEvents');
    }
    ngOnInit() {
        /* let daysFormGroup: FormGroup = new FormGroup({});
         for (let day of this.days) {
             let control: FormControl = new FormControl(day.value, Validators.required);
             daysFormGroup.addControl(day.value, control);
         }*/

        //create detailsForm FormGroup using FormBuilder's short-hand syntax
        /* this.defaultWorkingblockForm = this.formBuilder.group({
             name: ["", Validators.required],
             days: daysFormGroup
         });*/

        // this.selectedEmployee = Employee;

       //console.log('STarted WOrkingblock thingy now the id =....', this.selectedEmployee.EmployeeID);
       // this.loadCalendar(this.selectedEmployee);


    }
    momentClicked(){

    }

    eventClick() {

    }
    updateEvent(model: any) {
        model = {
            event: {
                id: model.event.id,
                start: model.event.start,
                end: model.event.end,
                title: model.event.title
                // other params
            },
            duration: {
                _data: model.duration._data
            }
        }
        this.displayEvent = model;


    }
}
