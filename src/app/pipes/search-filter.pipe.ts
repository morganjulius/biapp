import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchFilter'
})
export class SearchFilterPipe implements PipeTransform {

    transform(value: any, input: string) {
        if (input) {
            input = input.toLowerCase();
            return value.filter(function (el: any) {
                return el.FirstName.toLowerCase().indexOf(input) > -1 ||
                    el.LastName.toLowerCase().indexOf(input) > -1 ||
                    el.PhoneNumber.toLowerCase().indexOf(input) > -1  ||
                    el.Email.toLowerCase().indexOf(input) > -1 ;
            })
        }
        return value;
    }

}
