import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormsModule }from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';

import { TreatmentsService} from '../treatments.service';
import { Treatment, Product } from '../data-model';



@Component({
  selector: 'app-treatments',
  templateUrl: './treatments.component.html',
  styleUrls: ['./treatments.component.scss']
})
export class TreatmentsComponent implements OnInit {
  public treatments;
  public Etreatments;
    public selectedTreatment;
    events = [];
    opened: false;

  public treatments_error:Boolean = false;

  constructor(private _treatmentsService: TreatmentsService) { }


    onSelectTreatment(treatment: Treatment): void {
        this.selectedTreatment = treatment;
        console.log("Treatment selected", treatment);

    }

  getTreatmentsInternet() {
    this._treatmentsService.getTreatments().subscribe(
        data => {
          this.treatments = data;
          console.log("treatments :  : ", this.treatments);
          this.Etreatments = this.treatments.filter((item) => {
            return (item.OnLine === '1' && item.Deleted !== '1')
          });
          console.log("Etreatments :  : ", this.Etreatments);
        },
        err => {
          this.treatments_error = true;
          console.log('Something went wrong!');
        }
    );

  }

  ngOnInit() {
    this.getTreatmentsInternet();
  }

}
