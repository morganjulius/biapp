import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { ClientsService } from '../services/clients.service';
import { Client } from '../classes/clients';



@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent  implements OnInit {

    public clients;

    public selectedClient;

    events = [];
    public clients_error: Boolean = false;
    opened: false;
    searchText: '';

    constructor(private _clientsService: ClientsService) { }


    onSelectClient(client: Client): void {
        this.selectedClient = client;
        this.getClient(this.selectedClient.ClientID);
        console.log('Client selected from clients.ts', this.selectedClient.ClientID);

    }
    getClient(clientid) {
        console.log('Getting clientdetails in clientsComponent!', clientid);
        this._clientsService.getClient(clientid).subscribe(
            data => {
                this.clientDetails = data;
                console.log('Clients details clientsComponent.', data);


            },
            err => {
                this.clients_error = true;
                console.log('Something went wrong!');
            }
        );


    }
    getClients() {
        this._clientsService.getClients().subscribe(
            clientdata => {
                this.clients = clientdata;
                console.log("Allclients :  : ", this.clients);


            },
            err => {
                this.clients_error = true;
                console.log('Something went wrong!');
            }
        );

    }

    ngOnInit() {
        this.getClients();
    }

}