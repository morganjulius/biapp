/**
 * Created by julius on 10/12/2017.
 */
import { Component , OnInit, Input, OnChanges, Directive}              from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators, Validator, ValidatorFn }            from '@angular/forms';
import { HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import { TreatmentsService} from '../treatments.service';
import { ClientsService} from '../services/clients.service';
import { Treatment, Product } from '../data-model';
import {Freespace} from "../classes/freespace";
import {Client} from "../classes/clients";
import {FilterUniquePipe} from "../pipes/filter-unique.pipe";




@Component({
    selector: 'reservering',
    templateUrl: './reserveringdetails.component.html'
})

export class ReserveringDetailsComponent implements OnInit{
    public treatments;
    public freespaces;
    public Etreatments;
    public clientData: boolean = false;
    public treatments_error:Boolean = false;
    public loading = false;



    reserveringForm = new FormGroup ({
        treatmentid: new FormControl(),
        reservationoptions:new FormControl(),
        voornaam:new FormControl(),
        tussenvoegsel:new FormControl(),
        achternaam:new FormControl(),
        email:new FormControl(),
        Phone :new FormControl(),
        sidekick: new FormControl()
    });

    @Input() freespace: Freespace;
    @Input() client : Client[];

    constructor(private _treatmentsService: TreatmentsService,
                _clientsService: ClientsService
                ) {}

    getTreatmentsInternet() {
        this._treatmentsService.getTreatments().subscribe(
            data => {
                this.treatments = data;
                console.log("treatments :  : ", this.treatments);
                this.Etreatments = this.treatments.filter((item) => {
                    return (item.OnLine === '1' && item.Deleted !== '1')
                });
                console.log("Etreatments :  : ", this.Etreatments);
            },
            err => {
                this.treatments_error = true;
                console.log('Something went wrong!');
            }
        );

    }
    getFreeSpaces(treatmentid) {

        console.log('Something Started!', treatmentid);
        this.loading = true;
        this._treatmentsService.getFreeSpaces(treatmentid).subscribe(
            data => {
                this.freespaces = data;
                console.log("FreeSpace :  : ", this.freespaces);
                this.loading = false;


            },
            err => {
                this.treatments_error = true;
                console.log('Something went wrong getting avialable freespace!');
                this.loading = false;

            }
        );


    }
    getClientByPhone(newValue) {
        console.log("Get client by phoneNUmber" , newValue, this.client);
        if(newValue && newValue.length == '9') {
            console.log("Phonenumber is 10");
        }
    }

    getClientData() {
        this.clientData = true;

    }
    getTested(InputData) {
        console.log(InputData)
    }

    ngOnInit() {
        this.getTreatmentsInternet();

    }

}