import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReserveerComponent } from './reserveer.component';

describe('ReserveerComponent', () => {
  let component: ReserveerComponent;
  let fixture: ComponentFixture<ReserveerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReserveerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReserveerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
