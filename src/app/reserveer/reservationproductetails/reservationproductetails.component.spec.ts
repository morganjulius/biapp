import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationproductetailsComponent } from './reservationproductetails.component';

describe('ReservationproductetailsComponent', () => {
  let component: ReservationproductetailsComponent;
  let fixture: ComponentFixture<ReservationproductetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationproductetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationproductetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
