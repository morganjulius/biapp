import { Component, OnInit } from '@angular/core';
import { MessageserviceService } from '../messageservice.service';

@Component({
  selector: 'app-messageservice',
  templateUrl: './messageservice.component.html',
  styleUrls: ['./messageservice.component.scss']
})
export class MessageserviceComponent implements OnInit {

  constructor(public messageService: MessageserviceService) { }

  ngOnInit() {
  }
}
