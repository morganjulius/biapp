import { Component, OnInit, Input} from '@angular/core';
import { FormBuilder, FormControl, FormGroup }            from '@angular/forms';


import { Client } from '../classes/clients';


@Component({
    selector: 'app-clientdetails',
    templateUrl: './clientdetails.component.html',
    styleUrls: ['./clientdetails.component.scss']
})
export class ClientdetailsComponent implements OnInit {
    @Input() client: Client;



    clientForm = new FormGroup ({
        clientid: new FormControl(),
        clientname: new FormControl(),
        firstname: new FormControl(),
        prefix: new FormControl(),
        lastname: new FormControl(),
        phonenumber: new FormControl(),
        email: new FormControl(),
        memo: new FormControl()

    });

    constructor() { }



    public options: Object = {
        placeholderText: 'Klant memo veld!',
        charCounterCount: false,
        toolbarButtons: ['bold', 'italic', 'underline', 'align', 'undo']
    }

    ngOnInit() {
      /*  this.getClients();*/
    }

}

