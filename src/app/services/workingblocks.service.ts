import { Injectable } from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs/observable/of';
import { MessageserviceService } from '../messageservice.service';
import { Workingblock } from '../classes/workingblock';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class WorkingblocksService {


    private workingblocksUrl = 'https://dev.vectrus.nl/hl/api/v1/workingblocks/';
    //private freespaceUrl = 'https://dev.vectrus.nl/hl/api/v1/freespace/';
    private log(message: string) {
        this.messageService.add('Workingblock core service: ' + message);
    }

    constructor(
        private http:HttpClient,

        private messageService: MessageserviceService
    ) { }


    getWorkingblocks(EmployeeID): Observable<Workingblock[]> {
        return this.http.get<Workingblock[]>('https://dev.vectrus.nl/hl/api/v1/workingblocks/'+EmployeeID).pipe(
            tap(_ => this.log(`Fetched current getWorkingblocks for EMployeeID :${EmployeeID}`)),
            catchError(this.handleError('getWorkingblocks', []))
        );
    }

    getWorkingblock(WorkingblockID) {
        return this.http.get('https://dev.vectrus.nl/hl/api/v1/workingblock/'+WorkingblockID).pipe(
            tap(_ => this.log(`fetched Workingblock`)),
            catchError(this.handleError<Workingblock>(`Failed fetching single Workingblock`)

            )
        );



    }
    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
