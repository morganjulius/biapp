import { Injectable } from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs/observable/of';
import { MessageserviceService } from '../messageservice.service';
import { Client} from '../classes/clients';
import { catchError, map, tap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ClientsService {

    private clientsUrl = 'https://dev.vectrus.nl/hl/api/v1/clients/';
    //private freespaceUrl = 'https://dev.vectrus.nl/hl/api/v1/freespace/';


    private



    private log(message: string) {
        this.messageService.add('Client core service: ' + message);
    }

    constructor(
        private http:HttpClient,

        private messageService: MessageserviceService
    ) { }


    getClients(): Observable<Client[]> {
        return this.http.get<Client[]>('https://dev.vectrus.nl/hl/api/v1/clients/').pipe(
            tap(_ => this.log(`Fetched getClients `)),
            catchError(this.handleError('getClients', []))
        );
    }

    getClient(ClientID) {
        return this.http.get('https://dev.vectrus.nl/hl/api/v1/client/'+ClientID).pipe(
            tap(_ => this.log(`fetched Client`)),
            catchError(this.handleError<Client>(`Failed fetching single Client`)

            )
        );



    }
    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
