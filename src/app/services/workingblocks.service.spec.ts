import { TestBed, inject } from '@angular/core/testing';

import { WorkingblocksService } from './workingblocks.service';

describe('WorkingblocksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkingblocksService]
    });
  });

  it('should be created', inject([WorkingblocksService], (service: WorkingblocksService) => {
    expect(service).toBeTruthy();
  }));
});
