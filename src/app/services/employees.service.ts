import { Injectable } from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs/observable/of';
import { MessageserviceService } from '../messageservice.service';
import { Employee } from '../classes/employee';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class EmployeesService {

    private employeesUrl = 'https://dev.vectrus.nl/hl/api/v1/employees/';
    //private freespaceUrl = 'https://dev.vectrus.nl/hl/api/v1/freespace/';
    private log(message: string) {
        this.messageService.add('Employee core service: ' + message);
    }

    constructor(
        private http:HttpClient,

        private messageService: MessageserviceService
    ) { }


    getEmployees(): Observable<Employee[]> {
        return this.http.get<Employee[]>(this.employeesUrl).pipe(
            tap(_ => this.log(`Fetched current getEmployees `)),
            catchError(this.handleError('getEmployees', []))
        );
    }

    getEmployee(EmployeeID) {
        return this.http.get('https://dev.vectrus.nl/hl/api/v1/employee/'+EmployeeID).pipe(
            tap(_ => this.log(`fetched Employee`)),
            catchError(this.handleError<Employee>(`Failed fetching single Employee`)

            )
        );



    }
    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
