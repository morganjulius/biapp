import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {HttpModule} from '@angular/http';
import {of} from 'rxjs/observable/of';
import {MessageserviceService} from './messageservice.service';
import {catchError, map, tap} from 'rxjs/operators';
import {Reservations} from './data-model';




@Injectable()
export class ReservationsService {
    private reservationsUrl = 'https://dev.vectrus.nl/hl/api/v1/reservations';

    private log(message: string) {
        this.messageService.add('Reservations service: ' + message);
    }

    constructor(private http: HttpClient,
                private messageService: MessageserviceService
        ) {
    }


    getReservations(): Observable<Reservations[]> {
       return this.http.get<Reservations[]>(this.reservationsUrl).pipe(
            tap(_ => this.log(`Fetched current reservations `)),
            catchError(this.handleError('getReservations', []))
        );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
