export class Client {
    ClientID: number;
    ClientName: string;
    Firstname: string;
    Prefix: string;
    LastName: string;
    Email: string;
    Phone: number;
    colorCode: string;
    Memo: string;
    ReservationHistory: string;

}
