import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes }  from '@angular/router';

import { ReserveerComponent } from '../reserveer/reserveer.component';
import { AgendaComponent } from '../agenda/agenda.component';
import { TreatmentsComponent } from '../treatments/treatments.component';
import { ProductsComponent } from '../products/products.component';
import { ReserveringDetailsComponent } from '../reserveer/reserveringdetails.component';
import {ClientsComponent} from "../clients/clients.component";
import {EmployeesComponent} from "../employees/employees.component";
import {WorkingblocksComponent} from "../workingblocks/workingblocks.component";

const appRoutes: Routes = [
  { path: 'reserveren',        component: ReserveringDetailsComponent },
  { path: 'agenda',        component: AgendaComponent },
  { path: 'treatments',        component: TreatmentsComponent },
  { path: 'products',        component: ProductsComponent },
    { path: 'clients',        component: ClientsComponent},
    { path: 'employees',        component: EmployeesComponent },
    { path: 'workingblocks',        component: WorkingblocksComponent },
 { path: '',   redirectTo: 'reserveren', pathMatch: 'full' },

];

@NgModule({
  imports: [
    RouterModule.forRoot(
        appRoutes,
        { enableTracing: true } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}