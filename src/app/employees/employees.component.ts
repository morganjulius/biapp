import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup }            from '@angular/forms';
import { HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import { EmployeesService} from '../services/employees.service';
import { Employee } from '../classes/employee';
@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {
    public EmployeeID;
    public employees;
    public selectedEmployee: Employee;
    events = [];
    public employees_error:Boolean = false;
    constructor(private _employeesService: EmployeesService) { }
    opened: false;

    onSelectEmployee(employee: Employee): void {
        this.selectedEmployee = employee;
        console.log("Emplyeeselected", employee);

    }

    getEmployees() {
        this._employeesService.getEmployees().subscribe(
            data => {
                this.employees = data;
                console.log("employees :  : ", this.employees);


            },
            err => {
                this.employees_error = true;
                console.log('Something went wrong!');
            }
        );

    }

    ngOnInit() {
        this.getEmployees();
    }

}
