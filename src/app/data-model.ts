/**
 * Created by julius on 10/12/2017.
 */
import { ClientsService } from './services/clients.service';
import { Client } from './classes/clients';

export class Treatment {
    TreatmentID = 0;
    TreatmentName = '';
    Class = '';
    Costs = '';
    Internet = '';
    Products = Product;
}
export class Reservations {
    id: string;
    Duration: string;
    ProductCode: Treatment;
    title: string;
    start: string;
    end: string;
    CustomerData: Client;
    TreatmentName: Treatment;


}
export class Product {
    ProductID = '';
    Title   = '';
    ProductName   = '';
    Duration  = '';
    ProductCode    = '';
    Start    = '';
    End    = '';

}



export const states = ['NH', 'ZH', 'LI', 'ZE','GE', 'NB', 'FR', 'GR','UT', 'OV', 'DR', 'FL'];