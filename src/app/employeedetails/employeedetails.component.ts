import { Component, OnInit, Input} from '@angular/core';
import { FormBuilder, FormControl, FormGroup }            from '@angular/forms';
import { HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import { EmployeesService} from '../services/employees.service';
import { Employee } from '../classes/employee';
import { EmployeesComponent } from '../employees/employees.component';

@Component({
  selector: 'app-employeedetails',
  templateUrl: './employeedetails.component.html',
  styleUrls: ['./employeedetails.component.scss']
})
export class EmployeedetailsComponent implements OnInit {
    @Input() employee: EmployeesComponent;

    public employees: any;
     selectedEmployee: any = Employee;

    public employees_error:Boolean = false;

    employeeForm = new FormGroup ({
        employeeid: new FormControl(),
        employeename: new FormControl(),

        phone: new FormControl(),

        email: new FormControl()

    });



    constructor(private _employeesService: EmployeesService) { }



    getEmployee(employeeid) {
        console.log('Getting products for this treatment!');
        this._employeesService.getEmployee(employeeid).subscribe(
            data => {
                this.selectedEmployee = data;
                console.log(" Employees details", this.selectedEmployee);


            },
            err => {
                this.employees_error = true;
                console.log('Something went wrong!');
            }
        );


    }
    getEmployees() {
        this._employeesService.getEmployees().subscribe(
            data => {
                this.employees = data;
                console.log("employees :  : ", this.employees);


            },
            err => {
                this.employees_error = true;
                console.log('Something went wrong!');
            }
        );

    }

    ngOnInit() {
       // this.getEmployees();
    }

}
