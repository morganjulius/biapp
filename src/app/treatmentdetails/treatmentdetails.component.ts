import {Component,  Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup}            from '@angular/forms';

import {TreatmentsService} from '../treatments.service';

import {Treatment, Product} from '../data-model';
import {SortablejsModule} from 'angular-sortablejs';
import {SortablejsOptions} from 'angular-sortablejs';

import { TreatmentsComponent } from '../treatments/treatments.component';

@Component({
    selector: 'app-treatmentdetails',
    templateUrl: './treatmentdetails.component.html',
    styleUrls: ['./treatmentdetails.component.scss']
})



export class TreatmentDetailsComponent implements OnInit{
    @Input() treatment: TreatmentsComponent;

    public products_error: Boolean = false;

    public products;
    public selectedTreatmentProducts;




    addTo($event: any) {
       // this.treatment.Products.push($event.dragData);
    }


    constructor(private _treatmentsService: TreatmentsService) {

        if(this.treatment != null) {
            console.log('I have a treatment %o', this.treatment);

        }
    }


    getAllProducts() {
        console.log('Getting all available products!');
        this._treatmentsService.getAllProducts().subscribe(
            data => {
                this.products = data;
                console.log("Products :  : ", this.products);


            },
            err => {
                this.products_error = true;
                console.log('Something went wrong!');
            }
        );


    }
    getAllProductDetails(treatmentid) {
        console.log('Getting products for this treatment!', treatmentid);
        this._treatmentsService.getAllProductsFromTreatment(treatmentid).subscribe(
            data => {
                this.selectedTreatmentProducts = data;
                console.log("Products for this treatment  :  : ", this.selectedTreatmentProducts);


            },
            err => {
                this.products_error = true;
                console.log('Something went wrong!');
            }
        );

    }

    /*public addTo($event: any) {
        console.log("Event add to productlist triggered");
        //this.treatment.push($event.dragData);
    }*/

    public removeItem(item: any, list: any[]): void {
        list.splice(list.indexOf(item), 1);
    }

    ngOnInit() {
        this.getAllProducts();


    }

}
class Widget {
    constructor(public name: string) {}
}
