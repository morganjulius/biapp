import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreatmentdetailsComponent } from './treatmentdetails.component';

describe('TreatmentdetailsComponent', () => {
  let component: TreatmentdetailsComponent;
  let fixture: ComponentFixture<TreatmentdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreatmentdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatmentdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
