import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkingblocksComponent } from './workingblocks.component';

describe('WorkingblocksComponent', () => {
  let component: WorkingblocksComponent;
  let fixture: ComponentFixture<WorkingblocksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkingblocksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkingblocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
