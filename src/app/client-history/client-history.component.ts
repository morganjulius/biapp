import { Component, OnInit, Input} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ClientsService } from '../services/clients.service';

import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { Client } from '../classes/clients';


@Component({
  selector: 'app-client-history',
  templateUrl: './client-history.component.html',
  styleUrls: ['./client-history.component.scss']
})
export class ClientHistoryComponent implements OnInit {
    @Input() client: Client;


   // clientSelected = this.client;

    // initialize a private variable _data, it's a BehaviorSubject
    private _data = new BehaviorSubject<Client[]>([]);

    // change data to use getter and setter
    @Input()
    set data(value) {
        // set the latest value for _data BehaviorSubject
        this._data.next(value);
    }

    get data() {
        // get the latest value from _data BehaviorSubject
        return this._data.getValue();
    }




    constructor(private route: ActivatedRoute,
                private clientsService: ClientsService,
                private location: Location) {

    }

    ngOnInit(): void {
        this.getClient();

    }
    getClient(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.clientsService.getClient(id).subscribe(client => this.client = client);
    }
    goBack(): void {
        this.location.back();
    }

}
