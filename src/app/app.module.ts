import { BrowserModule } from '@angular/platform-browser';
import { NgModule, OnInit } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { ColorPickerModule } from 'ngx-color-picker';
/*import { DndListModule } from 'ngx-drag-and-drop-lists';*/
import {DndModule} from 'ng2-dnd';
import { LoadingModule, ANIMATION_TYPES  } from 'ngx-loading';
/*import { SpinnerModule } from '@chevtek/angular-spinners/dist';*/

/* Local language stuff */
import { registerLocaleData } from '@angular/common';
import localeNl from '@angular/common/locales/nl';

// the second parameter 'fr' is optional
registerLocaleData(localeNl, 'nl');

import { FullCalendarModule } from 'ng-fullcalendar';
import { AppRoutingModule } from './router/router.module';
import { TreatmentsService} from './treatments.service';
import { ReservationsService} from './reservations.service';
import { EmployeesService} from './services/employees.service';
import { ClientsService} from './services/clients.service';
import { WorkingblocksService} from './services/workingblocks.service';
import { MessageserviceService} from './messageservice.service';

import 'hammerjs';
import { SortablejsModule } from 'angular-sortablejs';
import 'rxjs/add/operator/map'
import {
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatStepperModule,
} from '@angular/material';

import { ModalModule } from '../../node_modules/ngx-bootstrap';

import { AppComponent } from './app.component';


import { ReserveerComponent } from './reserveer/reserveer.component';
import { ReserveringDetailsComponent } from './reserveer/reserveringdetails.component';
import { AgendaComponent } from './agenda/agenda.component';
import { TreatmentsComponent } from './treatments/treatments.component';
import { ProductsComponent } from './products/products.component';
import { TreatmentDetailsComponent } from './treatmentdetails/treatmentdetails.component';
import { MessageserviceComponent } from './messageservice/messageservice.component';
import { ClientsComponent } from './clients/clients.component';
import { EmployeesComponent } from './employees/employees.component';
import { WorkingblocksComponent } from './workingblocks/workingblocks.component';
import { EmployeedetailsComponent } from './employeedetails/employeedetails.component';
import { ClientdetailsComponent } from './clientdetails/clientdetails.component';
import { SearchFilterPipe } from './pipes/search-filter.pipe';
import { ReservationproductetailsComponent } from './reserveer/reservationproductetails/reservationproductetails.component';
import { DefaultworkingblocksComponent } from './defaultworkingblocks/defaultworkingblocks.component';
import { FilterUniquePipe } from './pipes/filter-unique.pipe';
import { ClientHistoryComponent } from './client-history/client-history.component';



@NgModule({
  declarations: [
    AppComponent,
    ReserveerComponent,
    ReserveringDetailsComponent,
    AgendaComponent,
    TreatmentsComponent,
    ProductsComponent,
    TreatmentDetailsComponent,
    MessageserviceComponent,
    ClientsComponent,
    EmployeesComponent,
    WorkingblocksComponent,
    EmployeedetailsComponent,
    ClientdetailsComponent,
    SearchFilterPipe,
    ReservationproductetailsComponent,
    DefaultworkingblocksComponent,
    FilterUniquePipe,
    ClientHistoryComponent

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FullCalendarModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    ColorPickerModule,
    DndModule.forRoot(),
      LoadingModule.forRoot({
          animationType: ANIMATION_TYPES.wanderingCubes,
          backdropBackgroundColour: 'rgba(0,0,0,0.1)',
          backdropBorderRadius: '4px',
          primaryColour: '#ddd',
          secondaryColour: '#555',
          tertiaryColour: '#f4f4f4'
      }),

    AppRoutingModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    SortablejsModule,

    FormsModule,
      MatAutocompleteModule,
      MatButtonModule,
      MatButtonToggleModule,
      MatCardModule,
      MatCheckboxModule,
      MatChipsModule,
      MatStepperModule,
      MatDatepickerModule,
      MatDialogModule,
      MatExpansionModule,
      MatGridListModule,
      MatIconModule,
      MatInputModule,
      MatListModule,
      MatMenuModule,
      MatNativeDateModule,
      MatPaginatorModule,
      MatProgressBarModule,
      MatProgressSpinnerModule,
      MatRadioModule,
      MatRippleModule,
      MatSelectModule,
      MatSidenavModule,
      MatSliderModule,
      MatSlideToggleModule,
      MatSnackBarModule,
      MatSortModule,
      MatTableModule,
      MatTabsModule,
      MatToolbarModule,
      MatTooltipModule,
  ],
  providers: [TreatmentsService, ReservationsService, EmployeesService, ClientsService, MessageserviceService, WorkingblocksService],
  bootstrap: [AppComponent]
})

export class AppModule { }
